package gp2;

import java.util.Scanner;

public class Ejercicio1 {
    public static void main(String[] args) {
          Scanner scan = new Scanner(System.in);
          
          System.out.println("Introduzaca un número entero para conocer su tabla de multiplicar.");
          
          int num = scan.nextInt();

          for (int i = 1; i <= 10; i++) {
              int result = i * num;
              System.out.println(num + " x " + i + " = " + result);
          }
          
    }   
}
