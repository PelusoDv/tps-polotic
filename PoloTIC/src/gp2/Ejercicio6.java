package gp2;

public class Ejercicio6 {

    public static void main(String[] args) {

        System.out.println("A continuación se mostrara un listado de los numeros primos menores a 200");

        int[] count = new int[200];

        for (int i = 2; i <= 200; i++) {
            count[i-2] = i;
        }
                       
        for (int n : count) {
            for (int i = (n - 1); i > 1; i--) {
                if (n % i == 0) {
                    count[n-2] = 0;
                }
            }
        }

        for (int i = 0; i < count.length; i++) {
            if (count[i] != 0) {
                System.out.print(count[i] + " ");
            }
        }
        
        System.out.println("");
    }
}
