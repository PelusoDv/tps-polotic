package gp2;

import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Escriba una palabra para saber si es un palindromo:");

        String palb = scan.next();
        String palbBack = "";
        char letra;

        for (int i = 0; i < palb.length(); i++) {
            letra = palb.charAt(i); 
            palbBack = letra + palbBack; 
        }
        
        if (palb.equalsIgnoreCase(palbBack)) {
            System.out.println("Efectivamente es un palindromo.");
        } else {
            System.out.println("Esta palabra no es un palindromo.");
        }

    }
}
