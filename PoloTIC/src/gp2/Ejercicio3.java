package gp2;

import java.util.Arrays;

public class Ejercicio3 {

    public static void main(String[] args) {

        int[] numbArray = {25,3,14,7,85,16543,16,27};
        System.out.println("El arreglo original de numeros es:");
        for (int numb : numbArray) {
            System.out.print(numb + " ");
        }
        
        Arrays.sort(numbArray);
        System.out.println("\nEl arreglo ordenado  de numeros es:");
        for (int numb : numbArray) {
            System.out.print(numb + " ");
        }
        
        System.out.println("");
    }
}
