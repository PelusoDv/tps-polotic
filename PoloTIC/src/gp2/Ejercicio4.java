package gp2;

import java.util.Scanner;

public class Ejercicio4 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Escriba un numero entero para conocer su factorial:");

        int numb = scan.nextInt();
        long fact = 1;

        for (int i = 1; i < numb; i++) {
            long mult = (i * fact);
            fact = fact + mult;
        }
        System.out.println("Su factorial es: " + fact);
    }
}
