package gp2;

import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio7 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("A continuación, se le asignara un animal de acuerdo a las siguientes 3 preguntas:");

        boolean[] animal = new boolean[3];
        String err = "";

        do {
            System.out.println("Primera pregunta: ¿El animal es herbívoro?");
            String herv = scan.next();
            if ("si".equalsIgnoreCase(herv) || "sí".equalsIgnoreCase(herv) || "yes".equalsIgnoreCase(herv)) {
                err = "";
                animal[0] = true;
            } else if ("no".equalsIgnoreCase(herv)) {
                err = "";
                animal[0] = false;
            } else {
                err = "Error";
                System.out.println(err);
            }
        } while ("Error".equals(err));

        do {
            System.out.println("Segunda pregunta: ¿El animal es mamífero?");
            String mam = scan.next();
            if ("si".equalsIgnoreCase(mam) || "sí".equalsIgnoreCase(mam) || "yes".equalsIgnoreCase(mam)) {
                err = "";
                animal[0] = true;
            } else if ("no".equalsIgnoreCase(mam)) {
                err = "";
                animal[1] = false;
            } else {
                err = "Error";
                System.out.println("false");
            }
        } while ("Error".equals(err));

        do {
            System.out.println("Tercera pregunta: ¿El animal es dom�stico?");
            String dom = scan.next();
            if ("si".equalsIgnoreCase(dom) || "sí".equalsIgnoreCase(dom) || "yes".equalsIgnoreCase(dom)) {
                err = "";
                animal[0] = true;
            } else if ("no".equalsIgnoreCase(dom)) {
                err = "";
                animal[2] = false;
            } else {
                err = "Error";
                System.out.println("false");
            }
        } while ("Error".equals(err));

        switch (Arrays.toString(animal)) {
            case "[true, true, false]" ->
                System.out.println("Su animal es un Alce");
            case "[true, true, true]" ->
                System.out.println("Su animal es un Caballo");
            case "[true, false, false]" ->
                System.out.println("Su animal es un Caracol");
            case "[false, false, false]" ->
                System.out.println("Su animal es un Cóndor");
            case "[false, true, true]" ->
                System.out.println("Su animal es un Gato");
            case "[false, true, false]" ->
                System.out.println("Su animal es un León");
            case "[false, false, true]" ->
                System.out.println("Su animal es un Pitón");
            case "[true, false, true]" ->
                System.out.println("Su animal es un Tortuga");
            default ->
                System.err.println("Ocurrio un error inesperado");
        }

    }

}
