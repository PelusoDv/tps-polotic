package gp2;

import java.util.Scanner;

public class Ejercicio5 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Escriba un numero entero para conocer secuencia de Fibonacci:");

        int numb = scan.nextInt();

        int f1 = 0;
        int f2 = 1;

        System.out.println("La secuencia (F) correspondiente al n�mero ingresado es:");

        if ( numb == 0 ) {
            System.out.println("F0 = " + f1);
        } else {
            System.out.println("F0 = " + f1 + "\nF1 = " + f2);

            for (int i = 1; i < numb; i++) {
                int f3 = f1 + f2;
                System.out.println("F" + (i + 1) + " = " + f3);
                f1 = f2;
                f2 = f3;
            }
        }
    }
}
