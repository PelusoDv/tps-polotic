package gp1;

import java.util.Scanner;

public class Ejercicio4 {
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Saludos, aquí promediaremos la estatura de 3 personas.\n"
                + "Introduzca la primer estatura.");
        float hgt1 = scan.nextFloat();
        
        System.out.println("Introduzca la segunda estatura.");
        float hgt2 = scan.nextFloat();
        
        System.out.println("Introduzca la tercer estatura.");
        float hgt3 = scan.nextFloat();
        
        System.out.print("El promedio total es de: ");
        System.out.printf( "%.2f", (hgt1 + hgt2 + hgt3) / 3 );
        System.out.print( "\n");
    }
    
}
