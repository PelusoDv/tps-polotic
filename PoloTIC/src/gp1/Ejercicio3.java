package gp1;

import java.util.Scanner;

public class Ejercicio3 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Saludos, ¿Qué cálculo desea realizar? \n"
                + "Para seleccionar un cálculo escriba su inicial. \n"
                + "Calculos permitidos: suma, resta, multiplicación, divición.");
        var cal = scan.next();

        System.out.println("Introduzca un número 'x'");
        float nx = scan.nextFloat();
        System.out.println("Introduzca un número 'y'");
        float ny = scan.nextFloat();

        switch (cal) {
            case "s" ->
                System.out.println("El resultado es: " + (nx + ny));
            case "r" ->
                System.out.println("El resultado es: " + (nx - ny));
            case "m" ->
                System.out.println("El resultado es: " + (nx * ny));
            case "d" ->
                System.out.println("El resultado es: " + (nx / ny));
            default ->
                System.err.println("El cálculo introducido es incorrecto");
        }

    }
}
