package gp1;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Ejercicio8 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("#.##");

        System.out.println("Saludos, esta vez convertiremos grados Celcius en sus compañeros:\n"
                + "Fahrenheit y Kelvin. Para ello\n"
                + "ingrese una temperatura:");
        float celc = scan.nextFloat();

        float kelv = celc + 273.15f;
        float fahr = (celc * 1.8f) + 32f;

        System.out.println("La temperatura de " + celc + "º Celcius en\n"
                + "Kelvin es:" + df.format(kelv) + "º\n"
                + "Fahrenheit es:" + df.format(fahr) + "º");
    }

}
