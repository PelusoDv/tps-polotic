package gp1;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Ejercicio6 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("#.##");
        
        System.out.println("Saludos, para saber el importe a pagar y descontado de un porducto\n"
                + "por favor, ingrese el monto del mismo:");
        float prec = scan.nextFloat();
        
        System.out.println("Ahora ingrese el porsentaje de descuento:");
        float porc = scan.nextFloat() /100;
        
        float pago = prec * (1 - porc);
        float desc = prec * porc;
        
        System.out.println("Uds debera pagar $" + df.format(pago) + " por su producto\n"
                + "y le fueron descontados $" + df.format(desc) + " en total.");
    }

}
