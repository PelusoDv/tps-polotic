package gp1;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Ejercicio5 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        DecimalFormat df = new DecimalFormat("#.####");

        System.out.println("Saludos, aquí calcularemos el area y perimetro de un circulo.\n"
                + "Pero para eso necesitamos saber su radio.\n"
                + "Introduzca el radio del circulo a calcular:");
        float rad = scan.nextFloat();

        float area = (float) (Math.pow(rad, 2) * Math.PI);
        float per = (float) (2 * Math.PI * rad);

        System.out.println("El area del circulo es: " + df.format(area));
        System.out.println("Y su perimetro es: " + df.format(per));

    }

}
