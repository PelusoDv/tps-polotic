package gp1;

import java.util.Scanner;

public class Ejercicio9 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Saludos, ingrese un monto en pesos para saber su valor en\n"
                + "Dólares, Euros, Reales y Guaraníes");
        float peso = scan.nextFloat();

        float dolar = peso / 231.68f;
        float euro = peso / 250.69f;
        float real = peso / 46.81f;
        float guar = peso * 31f;

        System.out.print("Su valor en Dólares es ");
        System.out.printf("%.2f", dolar );
        System.out.print("\n");
        System.out.print("Su valor en Euros es ");
        System.out.printf("%.2f", euro );
        System.out.print("\n");
        System.out.print("Su valor en Reales es ");
        System.out.printf("%.2f", real );
        System.out.print("\n");
        System.out.print("Su valor en Guaraníes es ");
        System.out.printf("%.2f", guar );
        System.out.print("\n");
    }
}
