package gp1;

import java.util.Scanner;

public class Ejercicio7 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Saludos, este es el programa misterioso\n"
                + "introduzca la edad de 2 personas y vea que pasa.\n"
                + "Primero la primera:");
        int edad1 = scan.nextInt();
        
        System.out.println("Segundo la segunda:");
        int edad2 = scan.nextInt();
        
        int x = edad2;
        edad2 = edad1;
        edad1 = x;
         
         System.out.println("Inequivocamente, Haz ingresado primero " + edad1 + " y luego " + edad2);        
    }
}
